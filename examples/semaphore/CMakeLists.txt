message(STATUS "Twist semaphore test example")

# List sources

# All tests target

add_executable(twist_semaphore_test_example test.cpp)
target_link_libraries(twist_semaphore_test_example twist)
