#include <twist/test/test.hpp>

#include <twist/test/util/race.hpp>
#include <twist/test/util/plate.hpp>

#include <twist/stdlike/atomic.hpp>
#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>

#include <chrono>

////////////////////////////////////////////////////////////////////////////////

// Poorly synchronized counting semaphore

class Semaphore {
 public:
  Semaphore(size_t initial)
    : permits_(initial) {
  }

  // -1
  void Acquire() {
    while (true) {
      std::unique_lock lock(mutex_);
      while (permits_.load() == 0) {
        has_permits_.wait(lock);
      }
      --permits_;
    }
  }

  // +1
  void Release() {
    ++permits_;
    has_permits_.notify_one();
  }

 private:
  twist::stdlike::atomic<size_t> permits_;
  twist::stdlike::mutex mutex_;
  twist::stdlike::condition_variable has_permits_;
};

////////////////////////////////////////////////////////////////////////////////

using namespace std::chrono_literals;

TEST_SUITE(TwistExamples) {
  TWIST_TEST_TL(Semaphore, 3s) {
    const static size_t kThreads = 2;

    Semaphore mutex{1};
    twist::test::util::Plate plate;  // Guarded by mutex

    twist::test::util::Race race;

    for (size_t i = 0; i < kThreads; ++i) {
      race.Add([&]() {
        while (twist::test::KeepRunning()) {
          mutex.Acquire();
          {
            // Critical section
            plate.Access();
          }
          mutex.Release();
        }
      });
    }

    race.Run();

    std::cout << "Critical sections: " << plate.AccessCount() << std::endl;
  }
}

RUN_ALL_TESTS()
