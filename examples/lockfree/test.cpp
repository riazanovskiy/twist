#include <twist/test/test.hpp>
#include <twist/test/assert.hpp>

#include <twist/stdlike/mutex.hpp>

#include <twist/test/util/lockfree.hpp>
#include <twist/test/util/race.hpp>

#include <twist/fault/adversary/lockfree.hpp>

#include <iostream>
#include <stack>
#include <optional>

////////////////////////////////////////////////////////////////////////////////

// Not lock-free
template <typename T>
class Stack {
 public:
  void Push(T value) {
    std::lock_guard guard(mutex_);
    items_.push(std::move(value));
  }

  std::optional<T> Pop() {
    std::lock_guard guard(mutex_);
    if (!items_.empty()) {
      T top(std::move(items_.top()));
      items_.pop();
      return top;
    } else {
      return std::nullopt;
    }
  }

 private:
  twist::stdlike::mutex mutex_;
  std::stack<T> items_;
};

////////////////////////////////////////////////////////////////////////////////

TEST_SUITE(TwistExamples) {

  // Report deadlock with fibers, hangs with threads
  TWIST_TEST_TL(LockFree, 5s) {
    twist::fault::SetAdversary(twist::fault::CreateLockFreeAdversary());

    twist::test::util::ReportProgress<Stack<int>> stack;

    twist::test::util::Race race;

    static const int kThreads = 4;

    for (int i = 0; i < kThreads; ++i) {
      race.Add([&, i]() {
        int value = i;
        while (twist::test::KeepRunning()) {
          stack->Push(value);
          value = *stack->Pop();
        }
      });
    }

    race.Run();
  }
}

RUN_ALL_TESTS()
