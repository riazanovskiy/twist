#pragma once

#include <twist/fault/adversary/adversary.hpp>

namespace twist::test::util {

template <typename T>
class ReportProgress {
  class Guard {
   public:
    Guard(T* object) : object_(object) {
    }

    T* operator->() {
      return object_;
    }

    ~Guard() {
      twist::fault::GetAdversary()->ReportProgress();
    }

   private:
    T* object_;
  };

 public:
  Guard operator->() {
    return {&object_};
  }

 private:
  T object_;
};

}  // namespace twist::test::util
