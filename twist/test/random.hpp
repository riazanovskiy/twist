#pragma once

#include <twist/fault/random/integer.hpp>

namespace twist::test {

// [0, max]
inline uint64_t RandomUInteger(uint64_t max) {
  return fault::RandomUInteger(max);
}

inline bool Random2() {
  return RandomUInteger(2) == 0;
}

}  // namespace twist::test
