#pragma once

#include <twist/fiber/runtime/fiber.hpp>
#include <twist/fiber/runtime/chrono.hpp>

#include <wheels/support/assert.hpp>

#include <queue>
#include <chrono>

namespace twist {
namespace fiber {

class SleepQueue {
 public:
  using Clock = FiberSteadyClock;
  using TimePoint = Clock::time_point;

 public:
  void Put(Fiber* f, wheels::Duration d);
  bool IsEmpty() const;
  Fiber* Poll(TimePoint now);
  TimePoint NextDeadLine() const;

 private:
  static TimePoint ToDeadLine(wheels::Duration d);

 private:
  struct Entry {
    Fiber* fiber_;
    TimePoint deadline_;

    bool operator<(const Entry& other) const {
      return deadline_ > other.deadline_;
    }
  };

 private:
  std::priority_queue<Entry> queue_;
};

}  // namespace fiber
}  // namespace twist
