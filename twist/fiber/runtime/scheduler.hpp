#pragma once

#include <context/context.hpp>

#include <twist/fiber/runtime/api.hpp>
#include <twist/fiber/runtime/fiber.hpp>
#include <twist/fiber/runtime/chrono.hpp>
#include <twist/fiber/runtime/sleep_queue.hpp>

#include <wheels/support/time.hpp>
#include <wheels/support/id.hpp>

#include <set>
#include <string_view>
#include <random>

namespace twist {
namespace fiber {

using FiberQueue = wheels::IntrusiveList<Fiber>;

class Scheduler {
 public:
  Scheduler(size_t seed = 42);

  // One-shot
  void Run(FiberRoutine init);

  // System calls

  void Spawn(FiberRoutine routine);
  void Yield();
  void SleepFor(wheels::Duration duration);
  void Suspend(std::string_view where);
  void Resume(Fiber* fiber);
  void Terminate();

  Fiber* GetCurrentFiber() const;

  VirtualTime::time_point Now() const {
    return time_.Now();
  }

  uint64_t GenerateRandomNumber();

  // Ignore upcoming Yield calls
  void PreemptDisable(bool disable = true) {
    preempt_disabled_ = disable;
  }

  // Statistics for tests
  size_t SwitchCount() const {
    return switch_count_;
  }

 private:
  void RunLoop();

  void CheckDeadlock();
  void ReportDeadlockAndDie();
  void ReportFromDeadlockedFiber(Fiber* fiber);

  bool IsIdle() const;
  void PollSleepQueue();
  void TimeKeeper();

  void Tick();

  // Context switches
  void SwitchToFiber(Fiber* fiber);
  // Precondition: me == GetCurrentFiber()
  void SwitchToScheduler(Fiber* me);

  Fiber* PickReadyFiber();
  // Context switch: scheduler -> fiber
  void Step(Fiber* fiber);
  // Handle system call in scheduler context
  void Reschedule(Fiber* fiber);
  // Add fiber to run queue
  void Schedule(Fiber* fiber);

  Fiber* CreateFiber(FiberRoutine routine);
  void Destroy(Fiber* fiber);

  size_t AliveCount() const;

 private:
  context::ExecutionContext loop_context_;  // Thread context

  FiberQueue run_queue_;
  Fiber* running_{nullptr};
  SleepQueue sleep_queue_;
  std::set<Fiber*> alive_;

  wheels::IdGenerator ids_;

  VirtualTime time_;
  std::mt19937_64 twister_;

  bool preempt_disabled_{false};

  // Statistics
  size_t switch_count_{0};
};

//////////////////////////////////////////////////////////////////////

Fiber* GetCurrentFiber();
Fiber* TryGetCurrentFiber();

Scheduler* GetCurrentScheduler();

}  // namespace fiber
}  // namespace twist
