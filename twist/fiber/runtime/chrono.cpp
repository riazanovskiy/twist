#include <twist/fiber/runtime/chrono.hpp>

#include <twist/fiber/runtime/scheduler.hpp>

#include <wheels/support/assert.hpp>

namespace twist::fiber {

//////////////////////////////////////////////////////////////////////

void VirtualTime::AdvanceTo(time_point tp) {
  WHEELS_VERIFY(tp >= now_nanos_, "Time cannot move backward");
  now_nanos_ = tp;
}

//////////////////////////////////////////////////////////////////////

using time_point = VirtualTime::time_point;

time_point FiberSteadyClock::now() {
  return GetCurrentScheduler()->Now();
}

}  // namespace twist::fiber
