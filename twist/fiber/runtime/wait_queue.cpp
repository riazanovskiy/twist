#include <twist/fiber/runtime/wait_queue.hpp>

#include <twist/fiber/runtime/scheduler.hpp>

#if defined(TWIST_FAULTY)
#include <twist/fault/random/helpers.hpp>
#endif

namespace twist {
namespace fiber {

static inline void Suspend(std::string_view where) {
  GetCurrentScheduler()->Suspend(where);
}

static inline void Resume(Fiber* f) {
  GetCurrentScheduler()->Resume(f);
}

class WaitQueue::Impl {
 public:
  Impl(const std::string& descr) : descr_(descr) {
  }

  void Park() {
    Fiber* caller = GetCurrentFiber();
    wait_queue_.PushBack(caller);
    Suspend(/*where=*/descr_);
  }

  void WakeOne() {
    if (wait_queue_.IsEmpty()) {
      return;
    }
#if defined(TWIST_FAULTY)
    Fiber* f = fault::UnlinkRandomItem(wait_queue_);
#else
    Fiber* f = wait_queue_.PopFront();
#endif
    Resume(f);
  }

  void WakeAll() {
#if defined(TWIST_FAULTY)
    auto shuffled = fault::ShuffleToVector(wait_queue_);
    for (Fiber* f : shuffled) {
      Resume(f);
    }
#else
    while (!wait_queue_.IsEmpty()) {
      Fiber* f = wait_queue_.PopFront();
      Resume(f);
    }
#endif
  }

  bool IsEmpty() const {
    return wait_queue_.IsEmpty();
  }

 private:
  const std::string descr_;
  FiberQueue wait_queue_;
};

WaitQueue::WaitQueue(const std::string& descr)
    : pimpl_(std::make_unique<Impl>(descr)) {
}

WaitQueue::~WaitQueue() {
}

void WaitQueue::Park() {
  pimpl_->Park();
}

void WaitQueue::WakeOne() {
  pimpl_->WakeOne();
}

void WaitQueue::WakeAll() {
  pimpl_->WakeAll();
}

bool WaitQueue::IsEmpty() const {
  return pimpl_->IsEmpty();
}

}  // namespace fiber
}  // namespace twist
