#include <twist/fiber/runtime/fiber.hpp>

#include <twist/fiber/runtime/scheduler.hpp>
#include <twist/fiber/runtime/stacks.hpp>

#include <wheels/support/compiler.hpp>
#include <wheels/support/exception.hpp>

namespace twist {
namespace fiber {

//////////////////////////////////////////////////////////////////////

Fiber::Fiber(FiberRoutine&& routine, context::Stack&& stack, FiberId id)
    : routine_(std::move(routine)),
      stack_(std::move(stack)),
      state_(FiberState::Starting),
      id_(id) {
  context_.Setup(stack_.View(), Trampoline);
}

void Fiber::Trampoline() {
  Fiber* fiber = GetCurrentFiber();

  fiber->Context().AfterStart();

  fiber->SetState(FiberState::Running);

  try {
    fiber->InvokeUserRoutine();
  } catch (...) {
    WHEELS_PANIC("Uncaught exception in fiber "
                 << fiber->Id() << ": " << wheels::CurrentExceptionMessage());
  }

  GetCurrentScheduler()->Terminate();  // never returns

  WHEELS_UNREACHABLE();
}

}  // namespace fiber
}  // namespace twist
