#pragma once

#include <twist/strand/tls.hpp>

#include <wheels/support/noncopyable.hpp>

namespace twist {
namespace strand {

/*
 * Usage:
 *
 * ThreadLocal<int> value{256};
 * Store: *value = 1024;
 * Load: int this_thread_value = *value;
 */

template <typename T>
class ThreadLocal: private wheels::NonCopyable {
 public:
  ThreadLocal(TLSManager::Ctor ctor, TLSManager::Dtor dtor) {
    Init(ctor, dtor);
  }

  explicit ThreadLocal(T default_value = T{}) {
    auto ctor = [default_value]() -> void* {
      return new T(default_value);
    };
    auto dtor = [](void* ptr) {
      delete (T*)ptr;
    };

    Init(ctor, dtor);
  }

  T& Get() {
    return *GetPointer();
  }

  // Access thread-local instance
  T& operator*() {
    return Get();
  }

  // Usage: thread_local_obj->Method();
  T* operator->() {
    return GetPointer();
  }

 private:
  void Init(TLSManager::Ctor ctor, TLSManager::Dtor dtor) {
    slot_index_ = TLSManager::Instance().AcquireSlot(ctor, dtor);
  }

  T* GetPointer() {
    T* data = (T*)TLSManager::Instance().Access(slot_index_);
    WHEELS_VERIFY(data != nullptr, "Not initialized");
    return data;
  }

 private:
  size_t slot_index_;
};

}  // namespace strand
}  // namespace twist
