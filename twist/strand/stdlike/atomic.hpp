#pragma once

#include <twist/strand/futex.hpp>

#include <atomic>

namespace twist::strand::stdlike {

template <typename T>
class atomic : public std::atomic<T> {  // NOLINT
 public:
  atomic() : std::atomic<T>() {
  }

  explicit atomic(T initial_value) : std::atomic<T>(initial_value) {
  }

  // NOLINTNEXTLINE
  void wait(T old) {
    // https://eel.is/c++draft/atomics.types.generic#lib:atomic,wait
    while (this->load() == old) {
      FutexWait(/*expected=*/old);
    }
  }

  // NOLINTNEXTLINE
  void notify_one() {
    FutexWakeOne();
  }

  // NOLINTNEXTLINE
  void notify_all() {
    FutexWakeAll();
  }

  // Direct access to futex syscall

  // Allows spurious wakeups
  void FutexWait(T expected);
  void FutexWakeOne();
  void FutexWakeAll();

 private:
  uint32_t* FutexAddr();
};

// Support for std::atomic<T>::wait from C++20

template <typename T>
uint32_t* atomic<T>::FutexAddr() {
  static_assert(std::is_same<T, uint32_t>::value, "Not supported");
  return reinterpret_cast<uint32_t*>(this);
}

template <typename T>
void atomic<T>::FutexWait(T expected) {
  strand::FutexWait(FutexAddr(), expected);
}

template <typename T>
void atomic<T>::FutexWakeOne() {
  strand::FutexWakeOne(FutexAddr());
}

template <typename T>
void atomic<T>::FutexWakeAll() {
  strand::FutexWakeAll(FutexAddr());
}

}  // namespace twist::strand::stdlike
