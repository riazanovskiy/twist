#pragma once

#include <chrono>

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/fiber/runtime/api.hpp>
#include <twist/fiber/stdlike/thread.hpp>

namespace twist::strand::stdlike {

using thread = fiber::ThreadLike;  // NOLINT

using thread_id = fiber::FiberId;  // NOLINT

extern const thread_id kInvalidThreadId;

namespace this_thread {

inline thread_id get_id() {  // NOLINT
  return fiber::GetFiberId();
}

// scheduling

inline void yield() {  // NOLINT
  fiber::Yield();
}

inline void sleep_for(wheels::Duration duration) {  // NOLINT
  fiber::SleepFor(duration);
}

}  // namespace this_thread

}  // namespace twist::strand::stdlike

#else

// native threads

#include <wheels/support/time.hpp>

#include <thread>

namespace twist::strand::stdlike {

using thread = ::std::thread;  // NOLINT

using thread_id = ::std::thread::id;  // NOLINT

extern const thread_id kInvalidThreadId;

namespace this_thread {

inline thread_id get_id() {  // NOLINT
  return ::std::this_thread::get_id();
}

// scheduling

inline void yield() {  // NOLINT
  ::std::this_thread::yield();
}

// NOLINTNEXTLINE
inline void sleep_for(wheels::Duration duration) {
  ::std::this_thread::sleep_for(duration);
}

}  // namespace this_thread

}  // namespace twist::strand::stdlike

#endif
