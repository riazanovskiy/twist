#include <twist/strand/futex.hpp>

#if defined(TWIST_FIBERS)

#include <twist/fiber/sync/futex.hpp>

namespace twist {
namespace strand {

int FutexWait(uint32_t* addr, uint32_t expected) {
  return fiber::FutexWait(addr, expected);
}

void FutexWakeOne(uint32_t* addr) {
  fiber::FutexWakeOne(addr);
}

void FutexWakeAll(uint32_t* addr) {
  fiber::FutexWakeAll(addr);
}

}  // namespace strand
}  // namespace twist

#else

#include <twist/thread/native_futex.hpp>

namespace twist {
namespace strand {

int FutexWait(uint32_t* addr, uint32_t expected) {
  return thread::FutexWait(addr, expected);
}

void FutexWakeOne(uint32_t* addr) {
  thread::FutexWake(addr, 1);
}

void FutexWakeAll(uint32_t* addr) {
  thread::FutexWake(addr, INT_MAX);
}

}  // namespace strand
}  // namespace twist

#endif