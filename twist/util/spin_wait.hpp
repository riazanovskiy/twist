#pragma once

#include <twist/strand/spin_wait.hpp>

namespace twist::util {

using strand::SpinWait;

}  // namespace twist::util
