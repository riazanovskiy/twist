#pragma once

#include <twist/strand/thread_local.hpp>

namespace twist::util {

using strand::ThreadLocal;

}  // namespace twist::util
