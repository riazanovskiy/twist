#include <twist/thread/native_futex.hpp>

#if LINUX

#include <unistd.h>
#include <sys/syscall.h>
#include <linux/futex.h>

namespace {

// There is no glibc wrapper for 'futex' syscall

int futex(unsigned int* uaddr, int op, int val, const struct timespec* timeout,
          int* uaddr2, int val3) {
  return syscall(SYS_futex, uaddr, op, val, timeout, uaddr2, val3);
}

}  // namespace

namespace twist {
namespace thread {

int FutexWait(uint32_t* addr, uint32_t expected) {
  return futex(addr, FUTEX_WAIT_PRIVATE, expected, nullptr, nullptr, 0);
}

int FutexWake(uint32_t* addr, size_t count) {
  return futex(addr, FUTEX_WAKE_PRIVATE, (int)count, nullptr, nullptr, 0);
}

}  // namespace thread
}  // namespace twist

#else

#pragma message("Futex is not implemented on current platform")

// TODO: emulate futex with mutex+condvar

#endif
