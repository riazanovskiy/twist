#pragma once

#include <twist/fault/wrappers/mutex.hpp>

#include <memory>

// std::unique_lock
#include <mutex>

namespace twist {
namespace fault {

class FaultyCondVar {
 public:
  using Lock = std::unique_lock<FaultyMutex>;

  FaultyCondVar();
  ~FaultyCondVar();

  void wait(Lock& lock);  // NOLINT

  template <class Predicate>
  void wait(Lock& lock, Predicate predicate) {  // NOLINT
    while (!predicate()) {
      wait(lock);
    }
  }

  void notify_one();  // NOLINT
  void notify_all();  // NOLINT

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace fault
}  // namespace twist
