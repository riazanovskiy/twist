#ifndef FAULTY_CONDVAR_IMPL
#error "Internal implementation file"
#endif

#include <twist/fault/adversary/inject_fault.hpp>
#include <twist/fault/random/helpers.hpp>

#include <twist/fiber/runtime/scheduler.hpp>

// Implementation for fibers (TWIST_FIBERS) execution backend

namespace twist::fault {

using namespace twist::fiber;

// Disable fiber preemption in current scope
struct PreemptionGuard {
  PreemptionGuard() {
    GetCurrentScheduler()->PreemptDisable(true);
  }

  ~PreemptionGuard() {
    GetCurrentScheduler()->PreemptDisable(false);
  }
};

class FaultyCondVar::Impl {
 public:
  void Wait(Lock& lock) {
    if (++wait_call_count_ % 13 == 0) {
      return;
    }

    InjectFault();
    {
      PreemptionGuard guard;
      lock.unlock();
    }
    Park();
    InjectFault();
    lock.lock();
  }

  void NotifyOne() {
    InjectFault();
    if (!wait_queue_.IsEmpty()) {
      ResumeOne();
    }
    InjectFault();
  }

  void NotifyAll() {
    InjectFault();
    ResumeAll();
    InjectFault();
  }

 private:
  void Park() {
    Fiber* caller = GetCurrentFiber();
    wait_queue_.PushBack(caller);
    GetCurrentScheduler()->Suspend("condvar");
  }

  void ResumeOne() {
    Fiber* f = UnlinkRandomItem(wait_queue_);
    GetCurrentScheduler()->Resume(f);
  }

  void ResumeAll() {
    auto shuffled = ShuffleToVector(wait_queue_);
    for (Fiber* f : shuffled) {
      GetCurrentScheduler()->Resume(f);
    }
  }

 private:
  fiber::FiberQueue wait_queue_;
  size_t wait_call_count_{0};
};

}  // namespace twist::fault
