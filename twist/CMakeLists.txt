# --------------------------------------------------------------------

get_filename_component(LIB_INCLUDE_PATH ".." ABSOLUTE)
get_filename_component(LIB_PATH "." ABSOLUTE)

file(GLOB_RECURSE LIB_CXX_SOURCES ${LIB_PATH}/*.cpp)
file(GLOB_RECURSE LIB_HEADERS ${LIB_PATH}/*.hpp)

add_library(twist STATIC ${LIB_CXX_SOURCES} ${LIB_HEADERS})
target_include_directories(twist PUBLIC ${LIB_INCLUDE_PATH})

target_link_libraries(twist PUBLIC wheels context pthread)
if(TWIST_PRINT_STACKS)
    target_link_libraries(twist PRIVATE backward)
    if(LINUX)
        target_link_libraries(twist PRIVATE bfd dl)
    endif()
endif()

# --------------------------------------------------------------------

if(TWIST_FAULTY)
    target_compile_definitions(twist PUBLIC TWIST_FAULTY=1)
endif()

if(TWIST_FIBERS)
    target_compile_definitions(twist PUBLIC TWIST_FIBERS=1)
endif()

if (TWIST_PRINT_STACKS)
    target_compile_definitions(twist PUBLIC TWIST_PRINT_STACKS=1)
endif()

# --------------------------------------------------------------------

# Linters

if(TWIST_DEVELOPER)

if(CLANG_FORMAT_TOOL)
    add_clang_format_target(
            twist_clang_format
            ${CMAKE_CURRENT_SOURCE_DIR}
            ${LIB_HEADERS} ${LIB_CXX_SOURCES})
endif()

if(CLANG_TIDY_TOOL)
    add_clang_tidy_target(
            twist_clang_tidy
            ${CMAKE_CURRENT_SOURCE_DIR}
            ${LIB_INCLUDE_PATH}
            ${LIB_HEADERS} ${LIB_CXX_SOURCES})
endif()

endif()
